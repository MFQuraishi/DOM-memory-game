const gameContainer = document.getElementById("game");
gameContainer.addEventListener('click', handleCardClick);

const COLORS = [
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "red",
    "blue",
    "green",
    "orange",
    "purple"
];

let score = 0;
let maxScore = COLORS.length / 2;


function shuffle(array) {
    let counter = array.length;

    while (counter > 0) {
        let index = Math.floor(Math.random() * counter);

        counter--;

        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

let shuffledColors = shuffle(COLORS);

function createDivsForColors(colorArray) {

    colorArray.forEach((color, index) => {
        const newDiv = document.createElement("div");
        newDiv.setAttribute("data-color", color);
        newDiv.setAttribute("data-state", "close");
        // newDiv.setAttribute("id", index);
        gameContainer.append(newDiv);
    });
}

// TODO: Implement this function!

let firstCard = null;

function handleCardClick(event) {
    card = event.target;
    if (card.getAttribute("data-state") !== "open") {
        card.style.backgroundColor = card.getAttribute("data-color");
        card.setAttribute("data-state", "open");
        if (firstCard === null) {
            firstCard = card;
        } else {
            let first = firstCard;
            let second = card;
            firstCard = null;
            if (first.getAttribute("data-color") === second.getAttribute("data-color")) {
                console.log("matched");
                score += 1;
                if (score === maxScore) {
                    document.getElementById("winning-screen").style.display = "flex";
                    setTimeout(() => location.reload(), 2000);
                }
            } else {
                console.log("not matched");
                setTimeout(() => {
                    resetCard(first);
                    resetCard(second);
                }, 400);
            }
        }
    }
}

function resetCard(card) {
    card.setAttribute("data-state", "close");
    card.style.backgroundColor = "unset";
}

// when the DOM loads
createDivsForColors(shuffledColors);